<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'text'
    ];
    public function setSlugAttribute (){
        return $this->attributes['slug'] = bcrypt($this->text . time());
    }

    public static function getAuthor(Article $article){
        $user = User::findOrFail($article->user_id);
        return $user;
    }
}
